# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias vpm='sudo vpm'
alias vpi='sudo vpm install'
alias vps='sudo vpm search'
alias vpr='sudo vpm remove'
alias vpu='sudo vpm update'
alias clr='clear'
alias fetch='neofetch'
alias ci='vim ~/.config/i3/config'
alias cb='vim ~/.bashrc'
alias dl='cd ~/Downloads'
alias doc='cd ~/Documents'
alias pic='cd ~/Pictures'
alias wlp='cd ~/Pictures/Wallpapers'
alias hm='cd ~/'
alias rb='source ~/.bashrc'
alias xres='vim ~/.Xresources'
alias rbwin='sudo grub-reboot 1 | sudo reboot now'
export PATH=~/bin/:$PATH

