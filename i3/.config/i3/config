#############################################################################################
# This config is written by Ultima for his i3 dotfiles.					    #
# If you want this to work with your i3 configuration, ensure that all programs		    #
# installed within the config are installed and properly configured on your system (if      #
# needed).										    #
# The config is vim-centric and designed to (try) and be logical and intuitive for anyone   #
# famliar with vimkeys.									    #
# This config is provided without any sort of warranty or technical support, so please      #
# use at your own risk.									    #
#############################################################################################

#############################################################################################
#Variables										    #
#############################################################################################

#sets the modifier key (think windows key)
set $mod Mod4

#############################################################################################
# i3 custom settings									    #
#############################################################################################

# Custom Settings (Please sort later...)
focus_follows_mouse no 
bindsym $mod+Tab workspace back_and_forth
bindsym $mod+Print exec --no-startup-id scrot ~/screenshot.png
new_window 1pixel

# External applications

# Custom launchers (Please PLEASE sort later...)
exec_always --no-startup-id ~/bin/alty 
exec --no-startup-id compton -CG --backend glx --vsync opengl-swc -b -D 0 --no-fading-openclose
exec_always feh --no-startup-id --bg-scale ~/Wallpapers/miles1.jpg
exec_always xfce4-notifyd --no-startup-id

#Configures monitor layout to match physical layout, uses xrandr (profile generated with arandr)
exec_always --no-startup-id xrandr --output DisplayPort-0 --mode 1280x1024 --pos 0x1080 --rotate normal --output DisplayPort-1 --mode 1280x1024 --pos 3200x1080 --rotate normal --output DisplayPort-2 --primary --mode 1920x1080 --pos 1280x1080 --rotate normal --output HDMI-A-0 --mode 2560x1080 --pos 960x0 --rotate normal

# start rofi (a program launcher) in drun mode
bindsym $mod+d exec --no-startup-id rofi -show drun 


#############################################################################################
#i3 commands										    #
#############################################################################################

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:System San Francisco Display.ttf 12 

# start a terminal
bindsym $mod+Return exec --no-startup-id i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

#Change focus between windows
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# split in horizontal orientation
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec i3-msg exit

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3blocks to display a workspace bar (plus the system information i3blocks
# finds out, if available)
bar {
        status_command i3blocks
}

#############################################################################################
#i3 workspace section									    #
#############################################################################################

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10
